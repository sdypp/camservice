﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace S3CamApi.Services
{
    public class RecieveService : IRecieveService
    {
        public string PostImage([FromForm] ImageFormModel std)
        {
            // Getting Image
            var image = std.Image;

            // Saving Image on Server
            if (image?.Length > 0)
            {
                var path = Directory.GetCurrentDirectory() +"\\files\\" + image.FileName;

                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    image.CopyTo(fileStream);
                }
                return path;

            }
            return null;
        }
    }
}
