﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace S3CamApi.Services
{
    interface IRecieveService
    {

        string PostImage([FromForm]ImageFormModel std);

    }

    public class ImageFormModel
    {
        public IFormFile Image { get; set; }
    }
}
