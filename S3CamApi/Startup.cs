using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.S3;
using CamService.Controllers;
using CamService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using S3CamApi.Services;

namespace S3CamApi
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSingleton<IS3Service, S3Service>();
            services.AddAWSService<IAmazonS3>();
            services.AddMvc(option => option.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
                endpoints.MapGet("/api/S3Bucket/AddFile/{bucketname}", async context =>
                {
                    context.Request.Form.TryGetValue("Image", out var image);
                    context.Request.RouteValues.TryGetValue("bucketname", out var bucket);
                    await new S3Service(new AmazonS3Client()).UploadFileAsync(bucket.ToString(), image);

                    //await context.Response.WriteAsync("Hello World!");
                });
                endpoints.MapGet("/api/Image/Upload/{cammId}", async context =>
                {
                    try
                    {
                        context.Request.RouteValues.TryGetValue("cammId", out var val);
                        var image = context.Request.Form.Files.GetFile("Image");
                        Console.WriteLine(val);
                        Console.WriteLine(image);
                        var response = await new RecieveController().Upload((string)val, new ImageFormModel() { Image = image }).ConfigureAwait(false);
                        //await context.Response.WriteAsync("Hello World!");

                    }
                    catch (Exception)
                    {

                    }
                });
            });
        }
    }
}
