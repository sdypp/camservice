﻿using Amazon.S3;
using CamService.Services;
using Microsoft.AspNetCore.Mvc;
using S3CamApi.Services;
using System;
using System.IO;
using System.Threading.Tasks;


namespace CamService.Controllers
{
    [Produces("application/json")]
    [Route("api/Image")]
    public class RecieveController : Controller
    {

        [HttpPost]
        [Route("Upload/{cammId}")]
        public async Task<IActionResult> Upload([FromRoute] string cammId, [FromForm]ImageFormModel std)
        {
            var response = new RecieveService().PostImage(std);

            await new S3Service(new AmazonS3Client()).UploadFileAsync(new S3CamApi.MyOptions().Bucket, response);

            System.IO.File.Delete(response);

            return Ok(std.Image.FileName);
        }

    }
}