﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CamService.Models;
using CamService.Controllers;
using System.Threading.Tasks;

namespace CamService.Services
{
    public interface IS3Service
    {
        Task<S3Response> CreateBucketAsync(string bucketName);
        Task UploadFileAsync(string bucketName);
    }
}
