﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace CamService.Models
{
    public class S3Response
    {
        internal string Message;
        internal HttpStatusCode Status;
    }
}